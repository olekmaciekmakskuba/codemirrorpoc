package pl.gda.pg.dev.portal.client.modules.editor.files;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.codemirror.CodemirrorModule;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.show.ShowFileEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.show.ShowFileHandler;

@Singleton
public class FilesController implements ShowFileHandler {

	@Inject
	private CodemirrorModule codemirrorModule;
	private EditorFile currentFile = new EditorFile();

	@Override
	public void onShowFile(ShowFileEvent event) {
		saveFile();
		replaceFile(event.getEditorFile());
	}

	private void replaceFile(EditorFile editorFile) {
		currentFile = editorFile;
		codemirrorModule.setText(editorFile.getContent());
		codemirrorModule.setHistory(editorFile.getHistory());
	}

	private void saveFile() {
		currentFile.setContent(codemirrorModule.getText());
		currentFile.setHistory(codemirrorModule.getHistory());
	}
}
