package pl.gda.pg.dev.portal.client.logger;

public class Logger {

	public static native void log(String log)/*-{
        console.log(log);
    }-*/;

	public static native void log(Object obj)/*-{
        console.log(obj);
    }-*/;
}
