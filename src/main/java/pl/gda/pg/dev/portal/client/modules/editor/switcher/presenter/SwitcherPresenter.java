package pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.SwitcherView;

public class SwitcherPresenter {

	private final SwitcherView view;

	@Inject
	public SwitcherPresenter(SwitcherView view) {
		this.view = view;
	}

	public Widget getView() {
		return view.asWidget();
	}

	public void addButton(SwitcherButtonPresenter pageButton) {
		view.addPageButton(pageButton.getView());
	}

	public void addAddButtonClickHandler(ClickHandler clickHandler) {
		view.addAddButtonHandler(clickHandler);
	}
}
