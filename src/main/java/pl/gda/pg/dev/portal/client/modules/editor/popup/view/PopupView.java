package pl.gda.pg.dev.portal.client.modules.editor.popup.view;

import com.google.gwt.user.client.ui.IsWidget;

public interface PopupView extends IsWidget {
	void show();

	void setPresenter(Presenter presenter);

	interface Presenter {
		void submit(String folder, String fileName);
	}
}
