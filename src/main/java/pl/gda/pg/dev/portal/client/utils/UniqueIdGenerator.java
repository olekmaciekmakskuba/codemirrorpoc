package pl.gda.pg.dev.portal.client.utils;

public class UniqueIdGenerator {
	private int nextId = 1;

	public int get() {
		return nextId++;
	}
}
