package pl.gda.pg.dev.portal.client.modules.editor.files;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.List;

@Singleton
public class FilesContainer {

	@Inject
	private FileCreator fileCreator;

	private List<EditorFile> files = Lists.newArrayList();

	public Optional<EditorFile> createFile(FileData fileData) {
		String fullName = fileData.getFullName();
		if (canCreateFile(fullName)) {
			EditorFile file = fileCreator.createFile(fileData);
			files.add(file);
			return Optional.of(file);
		}

		return Optional.absent();
	}

	private boolean canCreateFile(String fullName) {
		return FluentIterable.from(files).allMatch(createFileNamePredicate(fullName));
	}

	private Predicate<EditorFile> createFileNamePredicate(final String fullName) {
		Predicate<EditorFile> predicate = new Predicate<EditorFile>() {
			@Override
			public boolean apply(EditorFile input) {
				return input.getFullName().equals(fullName);
			}
		};

		return Predicates.not(predicate);
	}
}
