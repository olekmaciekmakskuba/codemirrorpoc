package pl.gda.pg.dev.portal.client.modules.editor.files.events.open;

import com.google.gwt.event.shared.GwtEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;

public class OpenFileEvent extends GwtEvent<OpenFileHandler> {

	public static final Type<OpenFileHandler> TYPE = new Type<>();

	private EditorFile file;

	public OpenFileEvent(EditorFile file) {
		this.file = file;
	}

	@Override
	public Type<OpenFileHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(OpenFileHandler handler) {
		handler.onOpenFile(this);
	}

	public EditorFile getEditorFile() {
		return file;
	}
}
