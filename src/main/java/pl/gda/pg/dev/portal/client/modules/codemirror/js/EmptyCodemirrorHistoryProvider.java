package pl.gda.pg.dev.portal.client.modules.codemirror.js;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.inject.Provider;

public class EmptyCodemirrorHistoryProvider implements Provider<CodemirrorHistory> {

	private final CodemirrorHistory history;

	public EmptyCodemirrorHistoryProvider() {
		Element element = Document.get().createElement("div");
		CodemirrorObject codemirrorObject = CodemirrorBuilder.build(element);
		history = codemirrorObject.getHistory();
	}

	@Override
	public CodemirrorHistory get() {
		return history;
	}
}
