package pl.gda.pg.dev.portal.client.modules.editor.files;

public class FileData {
	private String content;
	private String fileName;
	private String folder;

	public FileData() {
		this.content = "";
		this.fileName = "";
		this.folder = "";
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getFullName() {
		return folder + "/" + fileName;
	}
}
