package pl.gda.pg.dev.portal.client.modules.codemirror.js;

import com.google.gwt.core.client.js.JsType;

@JsType
public interface CodemirrorObject {

	public void setValue(String value);

	public String getValue();

	public void focus();

	public CodemirrorHistory getHistory();

	public void setHistory(CodemirrorHistory history);

	public void clearHistory();
}
