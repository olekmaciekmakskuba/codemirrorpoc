package pl.gda.pg.dev.portal.client.modules.editor.files.events.show;

import com.google.gwt.event.shared.EventHandler;

public interface ShowFileHandler extends EventHandler {
	void onShowFile(ShowFileEvent event);
}
