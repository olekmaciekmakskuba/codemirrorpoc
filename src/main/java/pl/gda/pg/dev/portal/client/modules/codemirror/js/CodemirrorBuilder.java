package pl.gda.pg.dev.portal.client.modules.codemirror.js;

import com.google.gwt.dom.client.Element;

public class CodemirrorBuilder {

	public static CodemirrorObject build(Element element) {
		return initCodemirrorNative(element);
	}

	private static native CodemirrorObject initCodemirrorNative(Element parent)/*-{
        return $wnd.CodeMirror(parent, {
            mode: "text/x-java",
            showCursorWhenSelecting: true,
            lineNumber: true,
            matchBrackets: true
        });
    }-*/;
}
