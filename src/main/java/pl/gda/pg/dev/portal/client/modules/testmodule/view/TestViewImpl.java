package pl.gda.pg.dev.portal.client.modules.testmodule.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.editor.EditorPlace;

@Singleton
public class TestViewImpl extends Composite implements TestView {

	private static ApplicationViewUiBinder uiBinder = GWT.create(ApplicationViewUiBinder.class);

	@UiTemplate("TestView.ui.xml")
	interface ApplicationViewUiBinder extends UiBinder<Widget, TestViewImpl> {
	}

	@UiField
	FlowPanel mainPanel;
	@Inject
	private PlaceController placeController;
	@Inject
	private EditorPlace editorPlace;

	public TestViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Inject
	private void init() {
		Button label = new Button("Click me!");
		label.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				placeController.goTo(editorPlace);
			}
		});
		mainPanel.add(label);
	}
}
