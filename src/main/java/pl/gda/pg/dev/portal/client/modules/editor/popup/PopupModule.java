package pl.gda.pg.dev.portal.client.modules.editor.popup;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;
import pl.gda.pg.dev.portal.client.modules.editor.files.FileData;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesContainer;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.open.OpenFileEvent;
import pl.gda.pg.dev.portal.client.modules.editor.popup.view.PopupView;

public class PopupModule implements PopupView.Presenter {

	private final PopupView view;
	private final FilesContainer filesContainer;
	private final EventBus eventBus;

	@Inject
	private PopupModule(PopupView view, FilesContainer filesContainer, EventBus eventBus) {
		this.view = view;
		this.filesContainer = filesContainer;
		this.eventBus = eventBus;
		view.setPresenter(this);
	}

	@Override
	public void submit(String folder, String fileName) {
		FileData data = new FileData();
		data.setFileName(fileName);
		data.setFolder(folder);

		Optional<EditorFile> file = filesContainer.createFile(data);
		if (file.isPresent()) {
			eventBus.fireEvent(new OpenFileEvent(file.get()));
		}
	}

	public void show() {
		view.show();
	}
}
