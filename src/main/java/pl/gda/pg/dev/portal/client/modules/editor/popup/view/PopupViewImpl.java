package pl.gda.pg.dev.portal.client.modules.editor.popup.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class PopupViewImpl extends Composite implements PopupView {

	private static PopupViewUiBinder uiBinder = GWT.create(PopupViewUiBinder.class);
	private Presenter presenter;

	@UiTemplate("PopupView.ui.xml")
	interface PopupViewUiBinder extends UiBinder<Widget, PopupViewImpl> {
	}

	@UiField
	DialogBox dialogBox;
	@UiField
	SubmitButton okButton;
	@UiField
	Button cancelButton;
	@UiField
	TextBox fileNameBox;
	@UiField
	TextBox folderBox;

	public PopupViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Inject
	private void init() {
		dialogBox.setAutoHideEnabled(true);
		createCancelButtonHandler();
		createOkButtonHandler();
		createTextFieldHandler();
		dialogBox.center();
	}

	private void createTextFieldHandler() {
		fileNameBox.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.getUnicodeCharCode() == KeyCodes.KEY_ENTER) {
					submit();
				}
			}
		});
	}

	private void createOkButtonHandler() {
		okButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				submit();
			}
		});
	}

	private void createCancelButtonHandler() {
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});
	}

	private void submit() {
		dialogBox.hide();
		presenter.submit(folderBox.getText(), fileNameBox.getText());
		fileNameBox.setText("");
		folderBox.setText("");
	}

	@Override
	public void show() {
		dialogBox.show();
		folderBox.setFocus(true);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
}
