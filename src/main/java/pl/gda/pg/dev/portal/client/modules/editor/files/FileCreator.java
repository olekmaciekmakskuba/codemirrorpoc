package pl.gda.pg.dev.portal.client.modules.editor.files;

import com.google.inject.Inject;
import com.google.inject.Provider;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.EmptyCodemirrorHistoryProvider;
import pl.gda.pg.dev.portal.client.utils.UniqueIdGenerator;

public class FileCreator {
	@Inject
	private Provider<EditorFile> fileProvider;
	@Inject
	private EmptyCodemirrorHistoryProvider emptyHistoryProvider;
	@Inject
	private UniqueIdGenerator uniqueIdGenerator;

	public EditorFile createFile(FileData data) {
		EditorFile file = fileProvider.get();
		file.setContent(data.getContent());
		file.setFileName(data.getFileName());
		file.setFolder(data.getFolder());
		file.setHistory(emptyHistoryProvider.get());
		file.setUniqueId(uniqueIdGenerator.get());
		return file;
	}
}
