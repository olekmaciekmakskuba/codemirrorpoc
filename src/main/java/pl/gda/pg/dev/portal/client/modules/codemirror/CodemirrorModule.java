package pl.gda.pg.dev.portal.client.modules.codemirror;

import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorBuilder;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorHistory;
import pl.gda.pg.dev.portal.client.modules.codemirror.js.CodemirrorObject;
import pl.gda.pg.dev.portal.client.modules.codemirror.view.CodemirrorView;

@Singleton
public class CodemirrorModule {

	private final CodemirrorView view;
	private final CodemirrorObject codemirror;

	@Inject
	public CodemirrorModule(CodemirrorView codemirrorView) {
		this.view = codemirrorView;
		codemirror = CodemirrorBuilder.build(view.getWrapperElement());
	}

	public Widget getView() {
		return view.asWidget();
	}

	public void setText(String content) {
		codemirror.setValue(content);
	}

	public String getText() {
		return codemirror.getValue();
	}

	public void setFocus() {
		codemirror.focus();
	}

	public CodemirrorHistory getHistory() {
		return codemirror.getHistory();
	}

	public void setHistory(CodemirrorHistory history) {
		codemirror.setHistory(history);
	}

	public void clearHistory() {
		codemirror.clearHistory();
	}
}
