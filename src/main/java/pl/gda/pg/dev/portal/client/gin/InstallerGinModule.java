package pl.gda.pg.dev.portal.client.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import pl.gda.pg.dev.portal.client.modules.application.gin.ApplicationGinModule;
import pl.gda.pg.dev.portal.client.modules.codemirror.gin.CodemirrorGinModule;
import pl.gda.pg.dev.portal.client.modules.editor.gin.EditorGinModule;
import pl.gda.pg.dev.portal.client.modules.testmodule.gin.TestGinModule;

public class InstallerGinModule extends AbstractGinModule {

	@Override
	protected void configure() {
		install(new GinModule());
		install(new ApplicationGinModule());
		install(new CodemirrorGinModule());
		install(new TestGinModule());
		install(new EditorGinModule());
	}
}
