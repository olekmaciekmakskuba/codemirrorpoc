package pl.gda.pg.dev.portal.client.modules.application.view;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

public interface ApplicationView extends IsWidget {

	AcceptsOneWidget getContent();
}
