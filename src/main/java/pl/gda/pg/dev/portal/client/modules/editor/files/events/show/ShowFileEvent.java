package pl.gda.pg.dev.portal.client.modules.editor.files.events.show;

import com.google.gwt.event.shared.GwtEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;

public class ShowFileEvent extends GwtEvent<ShowFileHandler> {

	public static final Type<ShowFileHandler> TYPE = new Type<>();

	private EditorFile file;

	public ShowFileEvent(EditorFile file) {
		this.file = file;
	}

	@Override
	public Type<ShowFileHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ShowFileHandler handler) {
		handler.onShowFile(this);
	}

	public EditorFile getEditorFile() {
		return file;
	}
}
