package pl.gda.pg.dev.portal.client.modules.editor.switcher.view.button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import pl.gda.pg.dev.portal.client.styles.EditorStyles;

public class SwitcherButtonViewImpl extends Composite implements SwitcherButtonView {

	private static SwitcherButtonUiBinder uiBinder = GWT.create(SwitcherButtonUiBinder.class);

	@UiTemplate("SwitcherButtonView.ui.xml")
	interface SwitcherButtonUiBinder extends UiBinder<Widget, SwitcherButtonViewImpl> {
	}

	@UiField
	FocusPanel button;

	private EditorStyles styles = EditorStyles.getInstance();

	public SwitcherButtonViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void addClickHandler(ClickHandler clickHandler) {
		button.addClickHandler(clickHandler);
	}

	@Override
	public void activate() {
		button.addStyleName(styles.switcherButtonActive());
	}

	@Override
	public void deactivate() {
		button.removeStyleName(styles.switcherButtonActive());
	}

	@Override
	public void setName(String name) {
		button.clear();
		button.add(new Label(name));
	}
}
