package pl.gda.pg.dev.portal.client.modules.editor;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;
import com.google.inject.Singleton;

@Singleton
public class EditorPlace extends Place {

	@Prefix("editor")
	public static class Tokenizer implements PlaceTokenizer<EditorPlace> {

		@Override
		public EditorPlace getPlace(String token) {
			return new EditorPlace();
		}

		@Override
		public String getToken(EditorPlace place) {
			return "";
		}
	}
}
