package pl.gda.pg.dev.portal.client.modules.editor.switcher.view.button;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.IsWidget;

public interface SwitcherButtonView extends IsWidget {
	void addClickHandler(ClickHandler clickHandler);

	void activate();

	void deactivate();

	void setName(String name);
}
