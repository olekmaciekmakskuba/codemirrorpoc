package pl.gda.pg.dev.portal.client.modules.editor;

import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import pl.gda.pg.dev.portal.client.content.ContentActivity;
import pl.gda.pg.dev.portal.client.modules.editor.files.FilesController;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.show.ShowFileEvent;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.SwitcherModule;
import pl.gda.pg.dev.portal.client.modules.editor.view.EditorView;

@Singleton
public class EditorActivity extends ContentActivity {

	private EditorView view;

	@Inject
	public EditorActivity(EditorView editorView, EventBus eventBus, SwitcherModule switcher, FilesController filesController) {
		this.view = editorView;
		view.setSwitcher(switcher.getView());
		eventBus.addHandler(ShowFileEvent.TYPE, filesController);
	}

	@Override
	public Widget getView() {
		return view.asWidget();
	}
}
