package pl.gda.pg.dev.portal.client.modules.editor.switcher.presenter;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import pl.gda.pg.dev.portal.client.modules.editor.files.EditorFile;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.show.ShowFileEvent;
import pl.gda.pg.dev.portal.client.modules.editor.files.events.show.ShowFileHandler;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.button.SwitcherButtonView;

public class SwitcherButtonPresenter implements ShowFileHandler {

	private final SwitcherButtonView view;
	private EditorFile file;

	@Inject
	public SwitcherButtonPresenter(SwitcherButtonView view) {
		this.view = view;
	}

	public void addClickHandler(ClickHandler clickHandler) {
		view.addClickHandler(clickHandler);
	}

	public Widget getView() {
		return view.asWidget();
	}

	public void initPresenter(EditorFile file) {
		this.file = file;
		view.setName(file.getFileName());
		view.activate();
	}

	@Override
	public void onShowFile(ShowFileEvent event) {
		if (event.getEditorFile() == file) {
			view.activate();
		} else {
			view.deactivate();
		}
	}
}
