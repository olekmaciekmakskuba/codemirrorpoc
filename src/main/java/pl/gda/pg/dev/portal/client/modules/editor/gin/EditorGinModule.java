package pl.gda.pg.dev.portal.client.modules.editor.gin;

import com.google.gwt.inject.client.AbstractGinModule;
import pl.gda.pg.dev.portal.client.modules.editor.popup.view.PopupView;
import pl.gda.pg.dev.portal.client.modules.editor.popup.view.PopupViewImpl;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.SwitcherView;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.SwitcherViewImpl;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.button.SwitcherButtonView;
import pl.gda.pg.dev.portal.client.modules.editor.switcher.view.button.SwitcherButtonViewImpl;
import pl.gda.pg.dev.portal.client.modules.editor.view.EditorView;
import pl.gda.pg.dev.portal.client.modules.editor.view.EditorViewImpl;

public class EditorGinModule extends AbstractGinModule {

	@Override
	protected void configure() {
		bind(EditorView.class).to(EditorViewImpl.class);

		bind(PopupView.class).to(PopupViewImpl.class);

		bind(SwitcherView.class).to(SwitcherViewImpl.class);
		bind(SwitcherButtonView.class).to(SwitcherButtonViewImpl.class);
	}
}
