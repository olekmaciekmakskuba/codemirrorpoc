package pl.gda.pg.dev.portal.client.modules.editor.files.events.open;

import com.google.gwt.event.shared.EventHandler;

public interface OpenFileHandler extends EventHandler {
	void onOpenFile(OpenFileEvent event);
}
