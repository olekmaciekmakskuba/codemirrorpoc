package pl.gda.pg.dev.portal.client.content;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import pl.gda.pg.dev.portal.client.modules.editor.EditorPlace;
import pl.gda.pg.dev.portal.client.modules.testmodule.TestPlace;

@WithTokenizers({ TestPlace.Tokenizer.class, EditorPlace.Tokenizer.class })
public interface ContentPlaceHistoryMapper extends PlaceHistoryMapper {
}
