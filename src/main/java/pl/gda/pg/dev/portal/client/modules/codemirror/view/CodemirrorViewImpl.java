package pl.gda.pg.dev.portal.client.modules.codemirror.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;

@Singleton
public class CodemirrorViewImpl extends Composite implements CodemirrorView {

	private static CodemirrorUiBinder uiBinder = GWT.create(CodemirrorUiBinder.class);

	@UiTemplate("CodemirrorView.ui.xml")
	interface CodemirrorUiBinder extends UiBinder<Widget, CodemirrorViewImpl> {
	}

	@UiField
	FlowPanel codemirrorWrapper;

	public CodemirrorViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Element getWrapperElement() {
		return codemirrorWrapper.getElement();
	}
}
