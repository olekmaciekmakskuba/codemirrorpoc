package pl.gda.pg.dev.portal.client.modules.codemirror.view;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.IsWidget;

public interface CodemirrorView extends IsWidget {
	Element getWrapperElement();
}
