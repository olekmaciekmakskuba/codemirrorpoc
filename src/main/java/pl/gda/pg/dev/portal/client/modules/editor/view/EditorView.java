package pl.gda.pg.dev.portal.client.modules.editor.view;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public interface EditorView extends IsWidget {

	void setSwitcher(Widget view);
}
