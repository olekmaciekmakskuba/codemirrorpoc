package pl.gda.pg.dev.portal.client.modules.testmodule;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;
import com.google.inject.Singleton;

@Singleton
public class TestPlace extends Place {

	@Prefix("test")
	public static class Tokenizer implements PlaceTokenizer<TestPlace> {

		@Override
		public TestPlace getPlace(String token) {
			return new TestPlace();
		}

		@Override
		public String getToken(TestPlace place) {
			return "";
		}
	}
}
