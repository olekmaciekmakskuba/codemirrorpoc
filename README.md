PoC aplikacji frontendowej portalu do tworzenia i rozwiązywania zadań dla programistów.
Projekt ma na celu przygotowanie ogólnej architektury frontendu aplikacji

* git clone ofc (SourceTree)
* zalecam używanie IntelliJ Ultimate (potrzebne do GWT) - licencja studencka ([formater](https://www.dropbox.com/s/uf0bdy9spu7k6hg/settings.jar?dl=0))
* JDK 1.7 (niestety GWT jeszcze nie wspiera składni 1.8 )
* należy pobrać [SDK GWT 2.7.0](http://www.gwtproject.org/versions.html) - w IntelliJ konieczne będzie podanie ścieżki do folderu
* projekt należy zaimportować jako projekt Gradle'a
* odświeżyć projekt z wbudowanego pluginu gradle'a 

SETUP

* File->Project Structure->Modules->GWTPortal-> plusik i wybieramy GWT (należy podać ścieżkę do lokalizacji ze ściągniętym SDK 2.7.0)
* Edit Configurations -> plusik -> GWT Configuration:
* name: wiadomo
* Module: GWTPortal
* Use Super Dev Mode -> check
* with JavaScript debugger -> check

BUILD

* należy wykonać komendę:
> gradlew dist
* wykonywane akcje podczas dista to: clean, test, build oraz kompilacja GWT
* wybudowana aplikacja pojawi się w folderze build/gwt/out

W razie problemów proszę o kontakt :)

Powiązane projekty:

* [GradleTestRunner](https://bitbucket.org/aleksanderkotbury/gradletestrunner)
* [vert.x-commons](https://bitbucket.org/aleksanderkotbury/vert.x-commons)
* [VertxBridge](https://bitbucket.org/aleksanderkotbury/vertxbridge)
* [Database](https://bitbucket.org/mkoziara/portaldatabase)
* [data-commons](https://bitbucket.org/mkoziara/data-commons)

aleksanderkotbury